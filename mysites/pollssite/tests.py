from django.test import TestCase
import datetime
from django.utils import timezone
from pollssite.models import Choice, Question
from django.urls import reverse


class QuestionModelTests(TestCase):
    def test_was_published_recently_with_futue_question(self):
        time = timezone.now() + datetime.timedelta(days=10)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)


def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)


class QuestionIndexViewTests(TestCase):
    def test_no_question(self):
        response = self.client.get(reverse('polls3:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        q = create_question(question_text="Past question", days=-30)
        q.choice_set.create(choice_text="choice 1", votes=0)
        q.save()
        response = self.client.get(reverse('polls3:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past question>'])

    def test_future_question(self):
        q = create_question(question_text="Future question", days=30)
        q.choice_set.create(choice_text="choice 1", votes=0)
        q.save()
        response = self.client.get(reverse('polls3:index'))
        self.assertContains(response, "No polls are available")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_future_and_past_question(self):
        q = create_question(question_text="Past question", days=-30)
        q.choice_set.create(choice_text="choice 1", votes=0)
        q.save()
        create_question(question_text="Future question", days=30)
        response = self.client.get(reverse('polls3:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past question>'])

    def test_two_past_question(self):
        q1 = create_question(question_text="Past question", days=-30)
        q1.choice_set.create(choice_text="choice 1", votes=0)
        q1.save()
        q2 = create_question(question_text="Recent question", days=0)
        q2.choice_set.create(choice_text="choice 1", votes=0)
        q2.save()
        response = self.client.get(reverse('polls3:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'],
                                 ['<Question: Recent question>', '<Question: Past question>'])

    def test_question_without_choice(self):
        create_question(question_text="Ques no choice 2", days=0)
        response = self.client.get(reverse('polls3:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_question_with_choice(self):
        q = create_question(question_text="Ques no choice 2", days=0)
        q.choice_set.create(choice_text="choice 1", votes=0)
        q.save()
        response = self.client.get(reverse('polls3:index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Ques no choice 2>'])


class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        future_question = create_question(question_text="Future question", days=5)
        url = reverse('polls3:detailofques', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_question(question_text="Past question", days=-5)
        url = reverse('polls3:detailofques', args=(past_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, past_question.question_text)


class QuestionResultiewTests(TestCase):
    def test_future_question(self):
        future_question = create_question(question_text="Future question", days=5)
        url = reverse('polls3:detailofques', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_question(question_text="Past question", days=-5)
        url = reverse('polls3:detailofques', args=(past_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, past_question.question_text)
