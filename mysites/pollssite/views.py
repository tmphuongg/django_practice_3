from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from .models import Question, Choice


class IndexView(generic.ListView):
    template_name = 'pollssite/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        question_list = Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')
        # print(question_list)
        latest_question_list = list()
        for question in question_list:
            choice_count = question.choice_set.count()
            # print("_________________")
            # print(choice_count)
            if choice_count != 0:
                latest_question_list.append(question)

        # print(latest_question_list)
        return latest_question_list


# def index(request):
#     print(request)
#     print('___________')
#     latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     context = {'latest_question_list': latest_question_list, }
#     return render(request, 'pollssite/index.html', context)  # finally return HttpResponse

class DetailView(generic.DetailView):
    model = Question
    template_name = 'pollssite/detail.html'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())
# def detail(request, question_id):
#     question = get_object_or_404(Question, pk=question_id)
#     mapping_variables = {'question': question, }
#     return render(request, 'pollssite/detail.html', mapping_variables)


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'pollssite/res.html'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())


# def results(request, question_id):
#     question = get_object_or_404(Question, pk=question_id)
#     mapping_variables = {'question': question, }
#     return render(request, 'pollssite/res.html', mapping_variables)
#

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choiceID'])
    except(KeyError, Choice.DoesNotExist):
        mapping = {'question': question, 'error_message': "You didn't select a choice to vote"}
        return render(request, 'pollssite/detail.html', mapping)
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls3:res', args=(question_id,)))
